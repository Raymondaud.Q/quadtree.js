// Quentin RAYMONDAUD

class AxisAlignedBoundingBox { 
	constructor(xy,width,height){
		this.xy = xy;
		this.width = width;
		this.height = height;
		this.top = this.xy[1] - this.height
		this.bot = this.xy[1] + this.height
		this.left = this.xy[0] - this.width
		this.right = this.xy[0] + this.width
	}

	// P5js show() function
	show(){ 
		if (noPhysics){
			rectMode(RADIUS)
			stroke(255,0,0,125)
			fill(0,0,0,0)
			rect(this.xy[0], this.xy[1], this.width, this.height)
			stroke(0)
			rectMode(CORNER)
		}
	}

	move(newXY){
		if (this.xy.length==2){
			this.xy = newXY
			this.top = this.xy[1] - this.height
			this.bot = this.xy[1] + this.height
			this.left = this.xy[0] - this.width
			this.right = this.xy[0] + this.width}
	}

	contains(point) {
		return (
			this.left <= point[0] && point[0] <= this.right &&
			this.top <= point[1] && point[1] <= this.bot);
	}

	intersects(aabb) {
		return !(
			this.right < aabb.left || 
			this.left > aabb.right ||
			this.bot < aabb.top || 
			this.top > aabb.bot);
	}
}

class AABBQuadTree extends AxisAlignedBoundingBox{
	constructor(xy,width,height){
		super(xy, width, height)
		this.ne = false
		this.nw = false
		this.se = false
		this.sw = false
		this.elems = []
		this.limitPerNode = 3
	}

	show(){
		if ( this.ne ){
			this.ne.show()
			this.nw.show()
			this.se.show()
			this.sw.show()}
		super.show()
	}

	subdivide(){
		this.nw = new AABBQuadTree([this.xy[0]-this.width/2,this.xy[1]-this.height/2], this.width/2, this.height/2)
		this.ne = new AABBQuadTree([this.xy[0]+this.width/2,this.xy[1]-this.height/2], this.width/2, this.height/2)
		this.sw = new AABBQuadTree([this.xy[0]-this.width/2,this.xy[1]+this.height/2], this.width/2, this.height/2)
		this.se = new AABBQuadTree([this.xy[0]+this.width/2,this.xy[1]+this.height/2], this.width/2, this.height/2)

		while ( this.elems.length > 0){
			let cnt = 0;
			let aabb = this.elems.pop()
			if ( this.nw.insert(aabb) ) cnt+=1
			if ( this.ne.insert(aabb) ) cnt+=1
			if ( this.sw.insert(aabb) ) cnt+=1
			if ( this.se.insert(aabb) ) cnt+=1
			if (cnt==0)
				throw RangeError('capacity must be greater than 0');}
	}

	insert(aabb){
		if (!this.intersects(aabb) || this.elems.indexOf(aabb)>0 )
		  return false;

		if (this.elems.length < this.limitPerNode && !this.nw ){
			this.elems.push(aabb);
			return true;
		}

		let cnt = 0
		if (!this.nw && this.width > this.limitPerNode*2  && this.height > this.limitPerNode*2)
			this.subdivide();

		if (this.nw){
			if (this.nw.insert(aabb)) cnt+=1;
			if (this.ne.insert(aabb)) cnt+=1;
			if (this.sw.insert(aabb)) cnt+=1;
			if (this.se.insert(aabb)) cnt+=1;}

		if( cnt != 0 )
			return true
		else
			throw RangeError('capacity must be greater than 0');

		return false;
	}

	remove(aabb){
		if (!this.intersects(aabb) )
			return false;

		let previousLength = this.elems.length

		for (let p = 0; p < this.elems.length; p++)
			if (this.elems[p] == aabb )
				this.elems.splice(p,1);

		if (!this.nw)
			return previousLength > this.elems.length
		
		return (this.ne.remove(aabb) ||
				this.nw.remove(aabb) ||
				this.se.remove(aabb) ||
				this.sw.remove(aabb))
	}

	AABBQuery(aabb){ // ToFix
		let pointsInRange = []

		if (!this.intersects(aabb) )
			return [];

		for (let p = 0; p < this.elems.length; p++)
			if (this.elems[p].intersects(aabb))
				pointsInRange.push(this.elems[p]);

		if (!this.nw)
			return pointsInRange;

		if (this.nw){
			pointsInRange = [...pointsInRange, ...this.ne.AABBQuery(aabb)];
			pointsInRange  = [...pointsInRange, ...this.nw.AABBQuery(aabb)];
			pointsInRange  = [...pointsInRange, ...this.se.AABBQuery(aabb)];
			pointsInRange  = [...pointsInRange, ...this.sw.AABBQuery(aabb)];}

		return pointsInRange;
	}
}